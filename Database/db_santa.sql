-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2017 at 11:59 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_santa`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `BlogId` int(11) NOT NULL,
  `Title` varchar(5000) NOT NULL,
  `Description` varchar(40000) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`BlogId`, `Title`, `Description`, `Photo`, `CreatedOn`) VALUES
(2, 'blog about me', '<b>Capt. Santa Bahadur Lamichhane,</b> a well-known name in the \r\nrecreational aviation sector of Nepal is one of the best pilots with \r\nmore than 11 years of experience in recreational aviation. He has in \r\nexcess of 6000 flights by different types of ultralights/microlights. He\r\n operates all types (weight shift and 3-axis) of ultralights and \r\nGyrocopters. Many domestic and international tourists flying with him \r\nnear the 8000m+ mountains (Annapurna, Dhaulagiri, Fishtail etc) \r\nappreciate for his professionalism and excellent flying skills and \r\nbecome more than happy when they fly with him. He can operate utralight \r\nin various proposes like banner towing, aerial photography, film \r\nshooting, air shows, flower dropping, sky diving, scientific research \r\nand many more.\r\n\r\n<br>', '1507012476_gkl8002.jpg', '2017-10-03 06:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `cp_admin`
--

CREATE TABLE `cp_admin` (
  `Id` int(11) NOT NULL,
  `Username` varchar(256) NOT NULL,
  `Password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_admin`
--

INSERT INTO `cp_admin` (`Id`, `Username`, `Password`) VALUES
(1, 'dipika', 'agrawal');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `GalleryId` int(11) NOT NULL,
  `Photo` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`GalleryId`, `Photo`) VALUES
(1, '20170101_081820.jpg'),
(2, '20170103_084029.jpg'),
(3, '20170104_091536.jpg'),
(4, '1507012476_gkl8002.jpg'),
(5, '_GKL8002.jpg'),
(6, '_GKL8025.jpg'),
(7, '20170101_081820.jpg'),
(8, '20170101_081821.jpg'),
(9, '20170103_084021.jpg'),
(10, '20170103_084029.jpg'),
(11, '20170104_091536.jpg'),
(12, '1507012476_gkl8002.jpg'),
(13, '_GKL8025.jpg'),
(14, '20170101_081820.jpg'),
(15, '20170101_081821.jpg'),
(16, 'IMG_2295.jpg'),
(17, 'IMG_22951.jpg'),
(18, '20170103_084029.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `introduction`
--

CREATE TABLE `introduction` (
  `IntroductionId` int(11) NOT NULL,
  `Introduction` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `introduction`
--

INSERT INTO `introduction` (`IntroductionId`, `Introduction`) VALUES
(1, '<p> <b>Capt. Santa Bahadur Lamichhane,</b> a well-known name in the recreational aviation sector of Nepal is one of the best pilots with more than 11 years of experience in recreational aviation. He has in excess of 6000 flights by different types of ultralights/microlights. He operates all types (weight shift and 3-axis) of ultralights and Gyrocopters. Many domestic and international tourists flying with him near the 8000m+ mountains (Annapurna, Dhaulagiri, Fishtail etc) appreciate for his professionalism and excellent flying skills and become more than happy when they fly with him. He can operate utralight in various proposes like banner towing, aerial photography, film shooting, air shows, flower dropping, sky diving, scientific research and many more.</p>\r\n                        <p>The first student of <b>Avia Club Flight</b> School, Capt. Santa is Nepal, the FIRST PILOT trained in the country on ultralight type aircraft. He got his flying license from CAA of Nepal in 15th March 2012 and got Flight Instructor Pilot Rating in November 2013 and he is having special authorizations for the Ground Instructor. Former Operations Director of Avia Club Nepal, Mr. Lamichhane is currently holding the same positions and belongs to Heli Air Nepal operating Gyrocopter. He is not only a flyer but also a great mechanic for the aircraft as he had been trained in many different courses like Rotax Engines (iRMT), Basic Electrical Wiring, Motorcycle Mechanic Training, General Mechanic Course, Handicrafts etc. Keen Learner, Mr. Santa had also been attended in many different trainings to make his career wider. Training in Costumer Care and Tourism, Personal and Professional Excellence, Exceptional Costumer Relationship and Management Skills, Effective Marketing, Selling and Managerial Skills, Exceptional Costumer and Modern Selling Techniques, Functions of Aeronautical Services, Aviation Safety and Safety Management System, Workplace Safety and Management etc are the trainings that he had been attended. As a pilot, he had participated in Himalayan Paramotor Yatra and Air Show ï¿½ 2011 and in the 1st Garuda Fun ï¿½ Himalayan Air Festival-2015 organized by Nepal Air Sports Association.</p>\r\n                        <p><b>Santa Bahadur Lamichhane,</b> lovingly called SANTA was born in 26th August 1988 to Lok Bahadur Lamichhane and Rubina Lamichhane. Excellent and memorable childhood he spent in the remote place of Nepal, Manbu-Gorkha (his birthplace) as he did not experience of being second in his school life and once the result took him to be TOPPER of Gorkha District in class 5.  He still remembers his dream to be a flyer and dedications for the dream when he was struggling hard to get his poor family to up.  He started his school from Shree Chandrodaya Primary School, Aarughat-01 and Shree Bhawani Secondary School, Aarughat-02, Gorkha, Amrit Science Campus (ASCOL)-Kathmandu for the Iï¿½Sc and Tribhuwan University (Prithbhi Narayan Campus - Pokhara) for the further more studies. He speaks Nepali, English, Russian and Hindi and understands a little Chinese Language.</p>\r\n                        <p>Santa utilizes his extra time for the social work as he is a Founder President of Sayapatri Youth Club - Gorkha, An adviser of Muskan Youth Club - Gorkha, An active Member of Gorkha Milan Samaj - Pokhara and the Founder of Rubina-Lok Welfare Fund - Manbu to support to the best students and a best teacher in the school. He actively involved and supported to the students and people affected by the tragic Gorkha Earthquake 2072.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `ReviewId` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `Message` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`ReviewId`, `Name`, `Email`, `Message`) VALUES
(1, 'Dipika', 'dagrawal@gmail.com', 'Hello');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `VideoId` int(11) NOT NULL,
  `Title` varchar(256) NOT NULL,
  `VideoLink` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`VideoId`, `Title`, `VideoLink`) VALUES
(1, 'Bepanha pyar', 'KxCjVIFxZNo'),
(2, 'Kal ho na ho', '1BWdglekty0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`BlogId`);

--
-- Indexes for table `cp_admin`
--
ALTER TABLE `cp_admin`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`GalleryId`);

--
-- Indexes for table `introduction`
--
ALTER TABLE `introduction`
  ADD PRIMARY KEY (`IntroductionId`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`ReviewId`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`VideoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `BlogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cp_admin`
--
ALTER TABLE `cp_admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `GalleryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `introduction`
--
ALTER TABLE `introduction`
  MODIFY `IntroductionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `ReviewId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `VideoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
