<?php require('AdminLTE/inc/config.php');
if(isset($_POST['btnSubmit'])){
             $Name = $_POST['txtName'];
             $Email=$_POST["txtEmail"];
	   $Message= $_POST["txtMessage"];
             $add_sql = $mysqli->query("INSERT INTO reviews SET Name='$Name',Email = '$Email',
	   Message='$Message'");
             if($add_sql = TRUE){
                 $successMsg = '<div class="alert alert-success">Successfully review Added</div>';
                 echo "<meta http-equiv='refresh' content='0'>";
                 echo "<script>alert('Thanks for your review.');
                       window.location.href='index.php';
                       </script>";
	   }
}
?>
<html class="no-js" lang="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Santa Bahadur Lamichhane Personal Page</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/font-icon.css">
<link rel="stylesheet" href="css/font-awesome.css">
</head>

<body>
<!-- header section -->
<section class="banner" role="banner" id="home"> 
  <!--header navigation -->
  <header id="header">
    <div class="header-content clearfix">
      <h2><a class="logo" href="#">Santa <span>Bahadur</span> Lamichhane</a></h2>
      <nav class="navigation" role="navigation">
        <ul class="primary-nav">
          <li><a href="#intro">Intro</a></li>
          <li><a href="#gallery">Gallery</a></li>
          <li><a href="#blog">Blog</a></li>
          <li><a href="#testimonials">Testimonials</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav>
      <a href="#" class="nav-toggle">Menu<span></span></a> </div>
  </header>
  <!--header navigation --> 
  <!-- banner text -->
  <div class="container">
    <div class="col-md-10 col-md-offset-1">
      <div class="banner-text text-center">
        <h1>Hi! I'm Santa Bdr lamichhane </h1>
        <p>the best pilots with more than 11 years of experience in recreational aviation. </p>
        <nav role="navigation"> <a href="#intro" class="banner-btn"><img src="images/down-arrow.png" alt=""></a></nav>
      </div>
      <!-- banner text --> 
    </div>
  </div>
</section>
<!-- header section --> 
<!-- about section -->
<section id="intro" class="section intro no-padding">
  <div class="container-fluid">
    <div class="row no-gutter">
      <div class="col-md-6">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <div class="avatar"> <img src="images/20170101_081820.jpg" alt="" class="img-responsive"> </div>
            </li>
            <li>
              <div class="avatar"> <img src="images/20170103_084029.jpg" alt="" class="img-responsive"> </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <blockquote>
          <h1>Introduction About Me</h1>
	         <div class="intro_content">
           <?php $latPackage=$mysqli->query("select * from introduction");
           $SiPackage=$latPackage->fetch_array();
           $Introduction=$SiPackage["Introduction"];
          ?>
          <?=$Introduction?>
        </div>
          <a href="#" class="btn btn-outline" data-toggle="modal" data-target="#detail">Read More About Me</a> 
          <!-- Modal -->
          <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Introduction About Me</h4>
                </div>
                <div class="modal-body">
                  <div id="scrollbar" class="scrollbar">
                    <div class="viewport">
                      <div class="overview">
                      <?=$Introduction?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </blockquote>
      </div>
    </div>
  </div>
</section>
<!-- about section --> 

<!-- work section -->
<section id="gallery" class="gallery section no-padding">
  <div class="container-fluid">
    <div class="row no-gutter">
     <?php  $latPackage=$mysqli->query("select * from gallery limit 12");
            while($SiPackage=$latPackage->fetch_array()){
             $GalleryId=$SiPackage["GalleryId"];
	   $Photo=$SiPackage["Photo"];
     ?>
      <div class="col-lg-2 col-md-4 col-sm-4 work"> 
       <a href="images/<?=$Photo?>" class="work-box"> <img src="images/<?=$Photo?>" alt="">
        <div class="overlay">
          <div class="overlay-caption">
            <p><span class="icon icon-magnifying-glass"></span></p>
          </div>
        </div>
       </a> </div>
    <?php } ?>
    </div>
    
    <div class="btn-wrapper"> <a href="#" class="btn btn-outline" id="scCollapAnchor" data-toggle="tooltip" data-placement="top" title="Expand All"></a></div>
    
    <div id="iiColapsable" style="display:none">
      <div class="row no-gutter">
      <?php $Gallery=$mysqli->query("select * from gallery limit 12,100");
            while($SiGallery=$Gallery->fetch_array()){
             $RestGalleryId=$SiGallery["GalleryId"];
	   $RestPhoto=$SiGallery["Photo"]; ?>
        <div class="col-lg-2 col-md-4 col-sm-4 work"> 
        <a href="images/<?=$RestPhoto?>" class="work-box"> <img src="images/<?=$RestPhoto?>" alt="">
          <div class="overlay">
            <div class="overlay-caption">
              <p><span class="icon icon-magnifying-glass"></span></p>
            </div>
          </div>
        </a> 
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</section>
<!-- work section --> 
<!-- our team section -->
<section id="blog" class="section blog">
  <div class="container">
    <div class="row"> 
      <?php $Gallery=$mysqli->query("select * from blog");
            while($SiGallery=$Gallery->fetch_array()){
             $BlogId=$SiGallery["BlogId"];
	   $Title=$SiGallery["Title"]; 
	   $Description=$SiGallery["Description"]; 
	   $Photo=$SiGallery["Photo"]; 
	   $CreatedOn=$SiGallery["CreatedOn"]; 
      ?>
      <div class="col-md-4 col-sm-8">
        <div class="person"> <img src="images/<?=$Photo?>" alt="" class="img-responsive">
          <div class="person-content">
            <h4><a href="#"><?=$Title?></a></h4>
            <h5 class="date"><i class="fa fa-calendar"></i><?=$CreatedOn?></h5>
            <div class="block_desc">
            <?=$Description?>
          </div>
          </div>
          	<div class="blog_btn"><a href="#" class="btn" data-toggle="modal" data-target="#blog-detail">View Detail</a>
            	<!-- Modal -->
<div class="modal fade" id="blog-detail" tabindex="-1" role="dialog" aria-labelledby="blog-detailLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="blog-detailLabel"><?=$Title?></h4>
      </div>
      <div class="modal-body">
      <img src="images/_GKL8002.jpg" alt="" class="img-responsive">
        <h4><?=$Title?></h4>
        <h5 class="date"><i class="fa fa-calendar"></i><?=$CreatedOn?></h5>
        <?=$Description?>
      </div>
      
    </div>
  </div>
</div>
            </div>
        </div>
      </div>
	  <?php } ?>
    </div>
  </div>
</section>
<!-- our team section --> 
<!-- Testimonials section -->
<section id="testimonials" class="section testimonials no-padding">
  <div class="container-fluid">
    <div class="row no-gutter">
      <div class="flexslider">
        <ul class="slides">
          <?php $Review=$mysqli->query("select * from reviews");
            while($SiReview=$Review->fetch_array()){
             $ReviewId=$SiReview["ReviewId"];
	   $Message=$SiReview["Message"]; 
	   $Name=$SiReview["Name"]; 
	   $Email=$SiReview["Email"];
	?>
          <li>
            <div class="col-md-12">
              <blockquote>
                <h1><?=$Message?></h1>
                <p><?=$Name?>,<?=$Email?></p>
              </blockquote>
            </div>
          </li>
       <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Testimonials section --> 

<!-- Footer section -->
<footer class="footer" id="contact">
  <div class="footer-top section">
    <div class="container">
      <div class="row">
        <div class="footer-col col-md-6">
          <h5>Contact Me</h5>
          <ul class="contact_info">
          	<li><i class="fa fa-map-marker"></i> Damside, Pokhara, Nepal</li>
            <li><i class="fa fa-phone"></i> 98532100987</li>
             <li><i class="fa fa-envelope"></i> lamichhanesanta@gmail.com</li>
          </ul>
          <h5>Share with Love</h5>
          <ul class="footer-share">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
          <p>Copyright © 2017 Santa Bahadur Lamichane. All Rights Reserved. Made with <i class="fa fa-heart pulse"></i> by <a href="#">Webpage Nepal</a></p>
        </div>
        
        <div class="footer-col col-md-6">
          <h5>Your review</h5>
        
  <form class="contact_form" method="POST">
    <div class="form-group">
      <i class="fa fa-user"></i>
      <input type="text" class="form-control" id="txtName" name="txtName" placeholder="Name">
    </div>
  
  <div class="form-group">
    <i class="fa fa-envelope"></i>
    <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Email">
  </div>
  <div class="form-group">
    <i class="fa fa-comment"></i>
    <textarea class="form-control" rows="3" id="txtMessage"  name="txtMessage" placeholder="Message"></textarea></div>
  <button type="submit" name="btnSubmit"  class="btn btn-default">Submit</button>
</form>
          
        </div>
      </div>
    </div>
  </div>
  <!-- footer top --> 
  
</footer>
<!-- Footer section --> 
<!-- JS FILES --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.flexslider-min.js"></script> 
<script src="js/jquery.fancybox.pack.js"></script> 
<script src="js/retina.min.js"></script> 
<script src="js/modernizr.js"></script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGe_F0BQzzAueb0B4ZAUTWPv3Nejp0a7o&callback=initMap"
    async defer></script>
    <script src="js/googlemap.js"></script> 
<script src="js/main.js"></script>
</body>
</html>
